package com.penhey.coffeine;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.Inet4Address;

public class ScreenSignUp extends AppCompatActivity {
    private TextView titleSignUp,message,logIn;
    private EditText editTextUser,editTextEmail,editTextPass,editTextConform;
    private Button btnSignIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_sign_up);
        titleSignUp = findViewById(R.id.titleSignUp);
        editTextUser = findViewById(R.id.eiditTextUsername);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPass = findViewById(R.id.editTextPassword);
        editTextConform = findViewById(R.id.conformPassword);
        btnSignIn = findViewById(R.id.btnSignIn);
        message = findViewById(R.id.messageSignUp);
        logIn = findViewById(R.id.logIn);

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}