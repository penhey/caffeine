package com.penhey.coffeine;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Log_In_Screen extends AppCompatActivity {
    private View image;
    private TextView titleSignIn,message,signUp;
    private EditText editTextUser, editTextPass;
    private Button btnSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_in_screen);
        image = findViewById(R.id.imageUser);
        titleSignIn = findViewById(R.id.titleSignIn);
        editTextUser = findViewById(R.id.eiditTextUsername);
        editTextPass = findViewById(R.id.editTextPass);
        btnSignIn = findViewById(R.id.btnSignIn);
        message = findViewById(R.id.Message);
        signUp = findViewById(R.id.signUp);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Create an Intent to navigate to the SignUpActivity (replace SignUpActivity.class with the actual class name)
                Intent signUpIntent = new Intent(Log_In_Screen.this, ScreenSignUp.class);
                // Start the SignUpActivity
                startActivity(signUpIntent);
            }
        });
    }
}